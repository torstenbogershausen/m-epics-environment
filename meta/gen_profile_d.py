#!/bin/env python2.7

"""Helper script to generate the ess_epics_env.sh file."""

from __future__ import print_function
import os
import platform
import argparse
import subprocess, shlex

template_name = os.path.join('..', 'scripts', 'epicsbootstrap', 'templates', 'ess_epics_env.sh.template')
SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))

def get_epics_host_arch():
    """Try to figure out the host architecture based."""
    (dist, version, _) = platform.linux_distribution()
    dist = dist.lower()
    if dist:
        if 'centos' in dist:
            return 'centos{}-{}'.format(version[0], platform.machine())
        elif 'scientific linux' in dist:
            return 'SL{}-{}'.format(version[0], platform.machine())
        elif dist in ('ubuntu', 'debian'):
            return '{}{}-{}'.format(dist, version[0], platform.machine())
    return '{}-{}'.format(platform.system().lower(), platform.machine())

def get_eee_version():
    """Call make on parent folder to figure out version."""
    return subprocess.check_output(shlex.split('make --no-print-directory -C {}/../ version'.format(SCRIPTDIR))).strip()


def main():
    """Main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--epics-host-arch', type=str)
    parser.add_argument('--epics-bases-path', type=str, default='/opt/epics/bases')
    parser.add_argument('--epics-modules-path', type=str, default='/opt/epics/modules')
    parser.add_argument('--epics-env-version', type=str)

    args = parser.parse_args()

    # Only use defaults if no argument is given
    epics_bases_path   = args.epics_bases_path
    epics_modules_path = args.epics_modules_path
    epics_host_arch    = args.epics_host_arch   if args.epics_host_arch   else get_epics_host_arch()
    epics_env_version  = args.epics_env_version if args.epics_env_version else get_eee_version()

    with open('ess_epics_env.sh', 'w') as profilefile, open(template_name, 'r') as templatefile:
        template = templatefile.read()
        profilefile.write(template.format(epics_host_arch=epics_host_arch,
                                          epics_bases_path=epics_bases_path,
                                          epics_modules_path=epics_modules_path,
                                          epics_env_version=epics_env_version))

if __name__=="__main__":
    main()
