#!/bin/sh
# ESS EPICS Environment profile
#
# Varmunge is a method for adding a path to PATH avoiding to add it multiple
# times. Inspired by pathmunge in /etc/profile
#
# 1 - Environment variable
# 2 - Path to add
# 3 - (after,before) Append or prepend to environment variable.
varmunge () {{
    case ":$(eval echo \$$1):" in
        *:"$2":*)
            ;;
        ::)
            eval $1=$2
            ;;
        *)
            if [ "$3" = "after" ] ; then
                eval $1=$(eval echo \$$1):$2
            else
                eval $1=$2:$(eval echo \$$1)
            fi
    esac
}}

# Set the EEE environment variables
export EPICS_BASES_PATH={epics_bases_path}
export EPICS_MODULES_PATH={epics_modules_path}
export BASE=3.14.12.5

# Set the EPICS environment variables
export EPICS_HOST_ARCH={epics_host_arch}
export EPICS_BASE=${{EPICS_BASES_PATH}}/base-${{BASE}}

# Set the location of "module.Makefile"
export EPICS_ENV_PATH=${{EPICS_MODULES_PATH}}/environment/{epics_env_version}/${{BASE}}/bin/${{EPICS_HOST_ARCH}}


# Source the bash completions for requireExec
[ -f "${{EPICS_ENV_PATH}}/requireExecCompletions" ] && source ${{EPICS_ENV_PATH}}/requireExecCompletions

# Add iocsh to path
varmunge PATH ${{EPICS_ENV_PATH}} before

# Add EPICS tools to path (caput, caget, ...)
varmunge PATH ${{EPICS_BASE}}/bin/${{EPICS_HOST_ARCH}} before

# Add pvAccess tools to path (pvput, pvget, ...)
varmunge PATH ${{EPICS_MODULES_PATH}}/pvAccessCPP/4.1.2/${{BASE}}/bin/${{EPICS_HOST_ARCH}} before

# Add path to python module for pvaccess bindings
varmunge PYTHONPATH ${{EPICS_MODULES_PATH}}/pvaPy/0.5.1/${{BASE}}/lib/{epics_host_arch} before

export PYTHONPATH
